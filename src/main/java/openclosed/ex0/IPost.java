package openclosed.ex0;

import single.responsibility.cohension.ex3.Database;

public interface IPost {
    void createPost(Database db, String postMessage);
    void addTags(Database db,String postMessage);
}
