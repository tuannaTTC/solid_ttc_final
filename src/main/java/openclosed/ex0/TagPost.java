package openclosed.ex0;

import single.responsibility.cohension.ex3.Database;

public class TagPost extends Post {
    @Override
    public void addTags(Database db, String postMessage) {
        db.addAsTag(postMessage);
    }
}
