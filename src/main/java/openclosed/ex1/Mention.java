package openclosed.ex1;

import single.responsibility.cohension.ex3.Database;

public class Mention extends Post {
    @Override
    public void createPost(Database db, String postMessage) {
        db.addAsMentionPost(postMessage);
    }

}
