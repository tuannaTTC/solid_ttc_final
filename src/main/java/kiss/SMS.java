package kiss;

public class SMS implements IntegrationHandler {

    private IntegrationHandler sms;

    public SMS (IntegrationHandler sms){
        this.sms = sms;
    }

    @Override
    public IntegrationHandler isType(String integration) {
        if (IntegrationFactory.SMS.equals(integration)){
            return sms;
        }
        else {
            return null;
        }
    }
}
