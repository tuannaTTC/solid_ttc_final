package kiss;

public class Email implements IntegrationHandler {

    private IntegrationHandler email;

    public Email (IntegrationHandler email){
        this.email = email;
    }

    @Override
    public IntegrationHandler isType(String integration) {
        if (IntegrationFactory.EMAIL.equals(integration)){
            return email;
        }
        else {
            return null;
        }
    }
}
