package kiss;

public class IntegrationFactory {

    public static final String EMAIL = "abc";
    public static final String SMS = "dce";
    public static final String PUSH = "glk";

    public IntegrationHandler getHandlerFor(IntegrationHandler integrationHandler, String integration) {
        IntegrationHandler resul;
        resul = integrationHandler.isType(integration);
        if (resul!= null){
            return resul;
        }
        else throw new IllegalArgumentException("No handler found for integration: " + integration);
    }

}