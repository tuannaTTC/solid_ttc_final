package kiss;

public class Push implements IntegrationHandler {

    private IntegrationHandler push;

    public Push (IntegrationHandler push){
        this.push = push;
    }

    @Override
    public IntegrationHandler isType(String integration) {
        if (IntegrationFactory.PUSH.equals(integration)){
            return push;
        }
        else {
            return null;
        }
    }
}
