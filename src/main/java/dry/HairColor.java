package dry;

public class HairColor {
    private String color;

    public HairColor() {
    }

    public HairColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
