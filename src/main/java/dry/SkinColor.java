package dry;

public class SkinColor {
    private String color;

    public SkinColor() {
    }

    public SkinColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
