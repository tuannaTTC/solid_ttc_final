package dry;

public class Person {
    String name;
    int age;
    Gender sex;
    HairColor hairColor;
    SkinColor skinColor;

    public Person() {
    }

    public Person(String name, int age, Gender sex, HairColor hairColor, SkinColor skinColor) {
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.hairColor = hairColor;
        this.skinColor = skinColor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Gender getSex() {
        return sex;
    }

    public void setSex(Gender sex) {
        this.sex = sex;
    }

    public HairColor getHairColor() {
        return hairColor;
    }

    public void setHairColor(HairColor hairColor) {
        this.hairColor = hairColor;
    }

    public SkinColor getSkinColor() {
        return skinColor;
    }

    public void setSkinColor(SkinColor skinColor) {
        this.skinColor = skinColor;
    }
}
