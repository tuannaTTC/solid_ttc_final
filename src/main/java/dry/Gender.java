package dry;

public class Gender {
    private String sex;

    public Gender() {
    }

    public Gender(String sex) {
        this.sex = sex;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
