package dry;

import java.util.ArrayList;
import java.util.List;

public class GoodParent {
    public List<Person> findAllGoodPerson(List<Person> persons) {
        List<Person> results = new ArrayList<Person>();
        for (Person person : persons) {
            if (person.name.startsWith("T")
                    && person.age > 25
                    && person.getHairColor().getColor().equals(Constant.HAIR_COLOR.BLACK)
                    && person.getSkinColor().getColor().equals(Constant.SKIN_COLOR.WHITE)
                    ) {
                results.add(person);
            }
        }
        return results;
    }
    
    public List<Person> findAllGoodMan(List<Person> persons, String gender){
        ArrayList<Person> results = new ArrayList<Person>();
        List<Person> goodPerson = findAllGoodPerson(persons);
        for (Person person : goodPerson) {
            if (person.getSex().getSex().equals(Constant.GENDER.MALE)) {
                results.add(person);
            }
        }
        return results;
    }

    public List<Person> findAllGoodGirl(List<Person> persons, String gender){
        ArrayList<Person> results = new ArrayList<Person>();
        List<Person> goodPerson = findAllGoodPerson(persons);
        for (Person person : goodPerson) {
            if (person.getSex().getSex().equals(Constant.GENDER.FEMALE)) {
                results.add(person);
            }
        }
        return results;
    }
}








