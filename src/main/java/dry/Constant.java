package dry;

public interface Constant {
    public interface HAIR_COLOR{
        final String RED = "RED";
        final String BLACK = "BLACK";
        final String YELLOW = "YELLOW";
    }

    public interface SKIN_COLOR{
        final String BLACK = "BLACK";
        final String BROWN = "BROWN";
        final String WHITE = "WHITE";
    }
    public interface GENDER{
        final String FEMALE = "FEMALE";
        final String MALE = "MALE";
        final String GAY = "GAY";
        final String LES = "LES";
    }

}
