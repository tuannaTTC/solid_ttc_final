package dip;

import openclosed.ex0.IPost;

import openclosed.ex0.Post;
import single.responsibility.cohension.ex3.Database;

public class OfficeUser {
    private IPost post = new Post();

    public void publishNewPost(Database db,String postMessage) {
        this.post.createPost(db,postMessage);
    }
}
