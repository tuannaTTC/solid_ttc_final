package single.responsibility.cohension.ex1;

public class ShowSquare extends SquareAreaCalculator {
    public void draw() {
        System.out.println("draw square now!!");
    }

    public void rotate() {
        System.out.printf("we rotate this square now!!");
    }

}
