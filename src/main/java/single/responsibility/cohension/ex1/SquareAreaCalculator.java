package single.responsibility.cohension.ex1;

public class SquareAreaCalculator {
    int size = 5;

    public int calculateArea() {
        int realSquare = size * size;
        return realSquare;
    }

    public int calculateP() {
        int realP = size * 4;
        return realP;
    }

}
